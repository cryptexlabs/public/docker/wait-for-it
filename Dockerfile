FROM debian:latest

COPY wait-for-it.sh /

RUN chmod +x /wait-for-it.sh

ENTRYPOINT ["/wait-for-it.sh"]